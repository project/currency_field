<?php

namespace Drupal\currency_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'currency_widget' widget.
 *
 * @FieldWidget(
 *   id = "currency_widget",
 *   label = @Translation("Currency widget"),
 *   field_types = {
 *     "currency_field"
 *   }
 * )
 */
class CurrencyWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $display = $items[$delta]->getFieldDefinition()->getSetting('display');
    $element['value'] = $element + [
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#options' => currency_field_currency_options(!$element['#required'], ['AlphabeticCode' => $display])
    ];

    return $element;
  }

}
